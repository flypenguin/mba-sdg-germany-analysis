import csv
from contextlib import contextmanager
from pathlib import Path
from typing import Union, Optional

import matplotlib as plt
import pandas as pd
from sklearn.linear_model import LinearRegression

DIR = Path("csv/dns")

ROW_TRANS = {
    "target_year": int,
    "target_value": float,
    "lower_is_better": lambda x: x.lower() == "true",
}
DEFAULT_TRANS = lambda x: x


@contextmanager
def dr(infile: Union[Path, str]):
    try:
        inf = open(infile, "r")
        dialect = csv.Sniffer().sniff(inf.read(1024))
        inf.seek(0)
        dr = csv.DictReader(inf, dialect=dialect)
        yield dr
    finally:
        inf.close()


def process_target(row):
    """
    Reads the CSV file indicated by row, and returns a tuple: UNIT, DATAFRAME.

    The dataframe has the columns 'year', and 'value.
    #:param row: The row from the _targets.csv file
    :returns: unit:str, DataFrame
    """
    # print(f"PROCESSING '{row['goal_code']}' ...")
    df = pd.read_csv(DIR / (row["goal_code"] + ".csv"), sep=",")
    df.rename(
        columns={
            "Time Series": "series",
            "Time series": "series",
            "Series": "series",
            "Year": "year",
            "Value": "value",
            "Units": "units",
            "Calculation method": "series",  # 17-1
        },
        inplace=True)

    keep_cols = ("year", "value")
    ignore_cols = ("year", "value", "units")

    # choose a specific series if needed
    if val := row["time_series"]:
        # print(f"    - Selecting series: '{val}'")
        df = df[df["series"].isin(val.split("|"))]
        df.drop(columns=("series"), inplace=True)
    elif "series" in df.columns.values:
        # print(f"    - Selecting series.isna()")
        df = df.loc[df["series"].isna()]
        df.drop(columns=("series"), inplace=True)

    # select all nan columns
    for val in df.columns.values:
        if val in ignore_cols:
            continue
        # print(f"    - Selecting isna() values for: {val}")
        df = df[df[val].isna()]

    # get the unit
    unit = df["units"].unique()[0]

    drop_cols = list(set(df.columns.values) - set(keep_cols))
    # print(f"    - Dropping columns: {','.join(drop_cols)}")
    df.drop(columns=drop_cols, inplace=True)

    df.dropna(inplace=True)

    return unit, df


def extrapolate(row: dict[str, Optional[str]], df: pd.DataFrame) -> float:
    # https://stackoverflow.com/a/57773572/902327
    X = df["year"].values.reshape(-1, 1)
    y = df["value"].values.reshape(-1, 1)
    model = LinearRegression().fit(X, y)
    line_values = model.predict(X)
    target_estimate = model.predict([[row["target_year"]]])
    return line_values, target_estimate[0][0]


def create_stats():
    with dr(DIR / "_targets.csv") as reader:
        lines = [line for line in reader]

    total_metrics = len(lines)

    for idx, row in enumerate(lines):
        row = {k: ROW_TRANS.get(k, DEFAULT_TRANS)(v) for k, v in row.items()}
        target_value = row["target_value"]
        target_year = row["target_year"]
        lower_is_better = row["lower_is_better"]
        goal_code = row["goal_code"]

        # if row["goal_code"] != "17-2": continue
        # if idx != 36: continue

        unit, df = process_target(row)

        prediction, target_estimate = extrapolate(row, df)

        if lower_is_better:
            if target_estimate > target_value * 1.1:
                estimation_color = "red"
                estimation_marker = "v"
            else:
                estimation_color = "green"
                estimation_marker = "^"
        else:
            if target_estimate < target_value * 0.9:
                estimation_color = "red"
                estimation_marker = "^"
            else:
                estimation_color = "green"
                estimation_marker = "v"

        print(row["goal_code"])

        x1, y1 = df['year'].iloc[0], prediction[0][0]
        x2, y2 = row["target_year"], target_estimate


        plt.subplot(ceil(total_metrics / 4), 4, idx+1)

        plt.title(row["goal_code"])
        plt.xlabel('year')
        plt.ylabel(unit)

        plt.scatter(df["year"], df["value"], color = 'gray')

        if len(df["year"] == 0):
           print(row["goal_code"])
           print(df["year"])


        plt.axline((x1, y1), (x2, y2), color="gray", linestyle="--")

        plt.plot(df["year"], prediction, color = "gray")

        plt.scatter(row["target_year"], target_estimate, color=estimation_color, marker=estimation_marker)
        plt.scatter(row["target_year"], row["target_value"], color=estimation_color, marker="D")

        plt.ylim(ymin=0)
        plt.yaxis.set_major_locator(MaxNLocator(integer=True))

        plt.show()
