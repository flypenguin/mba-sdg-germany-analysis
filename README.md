# Are we DNS yet?

A super stupid statistical analysis whether Germany will meet its own DNS goals in 2030.

Linear regression, based on the given targets on the web page.

If someone knows what "DNS" stands for, I'd appreciate a note.

## Sources used

- [**dns-indikatoren.de**](http://dns-indikatoren.de/en/) (tracks the "German Sustainable Development Strategy")

## Other sources for SDGs

- [sgdindex.org](https://dashboards.sdgindex.org/profiles/germany) (data for this site)
- [sdg-indikatoren.de](https://sdg-indikatoren.de/en/) (tracks the UN goals – apparently)
- [worldbank](https://data.worldbank.org/country/DE)